Features
========

Working with single markup file
-------------------------------
- rst and md file format possible
- live output preview
- document printing on system printer
- building a pdf output with rst2pdf (for rst file format)

Working with sphinx project
---------------------------
- no conf.py file needed, if it not exist then build in Tebe default one wiil be used (it is possible to choose a sphinx html theme)
- index.rst file can be created automatically
- sphinx html output preview
- building a pdf output with rst2pdf as sphinx pdf builder
- Tebe does not create any additional files in your sphinx content directory (it use system temporary files while working)

Tebe has also handy help content about markup syntax and sphinx - it is very helpful while writing.



