About Tebe
==========

What is
------------
Tebe is a simple but powerful editor for Markdown and reStructuredText markup languages with Sphinx and Rst2Pdf power included. It can be used as a text editor for creating articles or even books. You can also use it to work with yours sphinx docs you have for your project.

Mission
-------
The mission of the Tebe project is to provide a simple and practical tool that will interest non-programer people to use markup syntax and sphinx for any text document writing.

.. Stage of development
.. --------------------
.. At the moment Tebe available on `PyPI .. <https://pypi.python.org/pypi/tebe>`_ as alpha stage software.

Source
------
Please find `Tebe git reposytory  <https://bitbucket.org/lukaszlaba/tebe/src>`_ on bitbucket hosting service.

License
-------
Tebe is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

Tebe is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Foobar; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.

Copyright (C) 2017-2018 Łukasz Laba <lukaszlab@o2.pl>