Welcome to Tebe's documentation!

Tebe is a simple but powerful editor for Markdown and reStructuredText markup languages with Sphinx and Rst2Pdf power included. Tebe make writing content with markup languages and Sphinx realy easy!

.. figure:: tebe.png

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   about
   features
   install
   tutorial

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`