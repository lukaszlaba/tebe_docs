Installing
==========

Tebe need Python 2 engine to be run.

Requirements
------------

1. Python 2.7

2. Non-standard python dependences

  - `tebe <https://pypi.python.org/pypi/tebe>`_
  - `pyqt4 <https://www.riverbankcomputing.com/software/pyqt>`_
  - `sphinx  <http://www.sphinx-doc.org>`_
  - `rst2pdf <https://pypi.python.org/pypi/rst2pdf>`_
  - `docutils <https://pypi.python.org/pypi/decutils>`_
  - `recommonmark <https://pypi.python.org/pypi/recommonmark>`_

Installing process with Conda management system
-----------------------------------------------

There are different ways to create required Python environment. If you are python beginer use Conda according to the instructions below.

1. Download and install `Miniconda <http://conda.pydata.org/miniconda.html>`_ - choose Python 2.7

2. Run system command line (Windows command prompt or Linux terminal)

3. First make sure we have the current conda package by typing::

     conda upgrade conda

4. Install packages from Conda repository by typing::

     conda install pip pyqt=4.11

 (above pyqt version need to be specified since conda default is pyqt5)

5. Install packages from PIPy repository by typing::

     pip install tebe sphinx rst2pdf docutils recommonmark 

6. Clean after installation by typing::

    conda clean -t -p

   Required Python environment has been created

7. To run Tebe, execute the file ``tebe.py`` from ``..\Miniconda\Lib\site-packages\tebe``. Before you try double-clicking the ``tebe.py`` file, you must change the "Open with" in the file properties - set the ``python.exe`` from ``..\Miniconda`` directory as default. For easy run make shortcut on your system pulpit to ``tebe.py`` file.

Tebe upgrade
-------------

If new version of Tebe package available upgrade it by typing::

    pip install --upgrade tebe

You can see actually Conda environment package list by typing::

    conda list

OS compatibility
----------------
Windows (7) and Linux (xubuntu, fedora) tested.